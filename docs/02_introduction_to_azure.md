# Introduction to Azure

## Infos

* Microsoft public cloud
* Released in 2010
* Focused on PaaS Service (where AWS is focused on IaaS). But IaaS avaible to
* ~ 60 Azure Region
* Paired region: For increase avaibility, regions are paired. 
When a full region fails, the other one can fill its place

## Azure useful links
* [List of services](https://azure.microsoft.com/en-us/services/)
* [Azure portal](https://portal.azure.com)
* Region avaible using CLI or terraform:
* `westus,westus2,eastus,centralus,centraluseuap,southcentralus,northcentralus,westcentralus,eastus2,eastus2euap,brazilsouth,brazilus,northeurope,westeurope,eastasia,southeastasia,japanwest,japaneast,koreacentral,koreasouth,southindia,westindia,centralindia,australiaeast,australiasoutheast,canadacentral,canadaeast,uksouth,ukwest,francecentral,francesouth,australiacentral,australiacentral2,uaecentral,uaenorth,southafricanorth,southafricawest,switzerlandnorth,switzerlandwest,germanynorth,germanywestcentral,norwayeast,norwaywest,brazilsoutheast,westus3,eastusslv,swedencentral,swedensouth`

## Subscription Account differences

__Subscription__ contains resources you provision in the cloud
* VMs
* DBs
* Networks
* ...

Can be attached to several account

__Account__ is an identity with access to resources in the subscription
<br/>Can be attached to several subscription

## Resource Group

* Content for resources used for grouping resources by logical boundaries
* Free
* Examples:
  * Environment
    * Dev-Resource-Group
    * Test-Resource-Group
    * Prod-Resource-Group
  * Teams


__Sum Up schema__
![Resource group](images/scope-levels.png)

__Create resource group using CLI__
* Create resource group: `az group create -l francecentral -n CLITest-rg`
* Delete resource group: `az group delete -n CLITest-rg`

## Storage Account
Resource used to store almost anything in Azure:
* Database backups
* VM Disks
* Diagnostic Datas
* ...
* Used for explicit data storage
* Quite Cheap

## SLA: Service Level Agreement

Define a Yearly Downtime Allowed.
<br/>__ALWAYS need to be check !__

| SLA (%) | Yearly Downtime Allowed |
|---------|-------------------------|
| 95      | 18d 6h 17m 27s          |
| 99      | 3d 15h 39m 29s          |
| 99.9    | 8h 45m 56s              |
| 99.99   | 52m 35s                 |


* SLA calculator: https://uptime.is

## Pricing model
* Per resource (ie. VM)
* Per consumption (ie. Function Apps)
* Reservations: paying upfront ex: for 3 years
* ALWAYS check resource's cost before using it
* calculator: https://azure.microsoft.com/en-us/pricing/calculator/


Architects skills:
* Standards:
  * Non-Functional Requirements
  * Technology Stack
  * Component's Architecture
  * Communication Pattern
* Cloud skills:
  * Infrastructure knowledge
  * Security
  * Hands-on (Cloud experience)

