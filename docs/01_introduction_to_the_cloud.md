# Introduction to the cloud

## Cloud's characteristics

* On demand self service
* Broad Network access
  * Different location access
* Resource pooling
  * Servers/VMs sharing
  * some advanced cloud services allow for physical resource separation (for security reasons)
* Rapid Elasticity
  * Set up and down automatically -> Terraform
* Measured Service
  * Payment for the used resources


## Financial aspect

__CapEx: Capital Expense__
<br/>Upfront investments for future uses

__OpEx: Operating Expense__
<br/>Payment for what you use


* Traditional IT: CapEx oriented (servers investments...)
* Cloud : OpEx oriented

## Cloud: *aaS

### IaaS: Infrastructure As A Service
Cloud provides:
* Compute
* Networking
* Storage

Minimum infrastructure.
<br/>Common example: Virtual Machine

### PaaS: Platefrom As A Service
Cloud provides:
* Compute
* Networking
* Storage
* Runtime environment
* Scaling
* Redundancy
* Security
* Updates
* Patching
* Maintenance

Client brings the code.
<br/>Examples:
* Web apps

### SaaS: Software as a service

Software are directly used by the client
<br/>Example:
* Office 365
* salesforce


### Sum up schema

![cloud-aas](images/cloud-aaS.jpg)

### Other services
* FaaS: Function As A Service
* DBaaS: DataBase As A Service
* IOTaaS: Internet Of Things As A service
* AIaaS: AI As A Service
* ...


## Type of cloud

### Public
* AWS
* Azure
* Google Cloud
* Access: External

### Private
* Example: Fitness
* Access: Internal

### Hybrid
Cloud set on organization's premises connected to the public cloud.
<br/>Example: 
* sensitive data : Private
* public data : Public