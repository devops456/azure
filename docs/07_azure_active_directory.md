# Azure Active Directory

* Central identity and access managment cloud service
* Used to manage access to thousands of apps
  * Azure Portal
* Secure, robust, intellignet
* Advanced feature
  * Multi Factor Authentication (MFA)
  * Conditional Access
  * Device Management
  * Hybrid identity
  * Identity protection
  * Monitoring and reports
  * ...
* Control access to Azure resources:
  * setting up, users, groups, roles

## Tenant
* Specific instance of Azure AD
* Called also Directory
* not part of the subscription
  * Exists beside the subscription
  * For new subscription, a new tenant is created automatically
* A tenant can be assigned to multiple subscription

