# Introducing our app

## ReadIt !
* Your bookshop on the cloud
* 4 main services:
  * Books Catalog
  * Shopping Cart
  * Inventory management
  * Order Engine
* Stack
  * .NET
  * nodeJS
* Uses VSCode
* Uses Various DataBases

## Plan
1. Begin with the basic app, run it locally
2. Move it to the cloud
3. Add networking and security
4. Add DB support
5. Add messaging
6. Discuss various alternatives for deployment
7. Keep an eye on the cost...

## Requirement

1. Only for the tutorial: Install dotnet SDK version 3.1: https://docs.microsoft.com/fr-fr/dotnet/core/install/linux-ubuntu
2. Install VSCode
3. Useful VSCode plugin
  * C#
  * Azure Account
  * Azure App Service
