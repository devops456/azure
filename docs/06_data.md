# Data in Azure

Azureprovides a lot of data solution as cloud service. They are fully managed by Azure
<br/>Prefer to use data service provide by azure

## Major Database feature
* Security (Network isolation, Encryption)
* Backup (Type, retention period)
* Aailability (SLA, replication, DR)

## Database on VM

If you to manage a Database and your own in an Azure VM, be aware that Azure offers the possiblity to setup a pre-installed VM (Example: Oracle VM)

__Pros:__
* Full flexibility
* Full control

__Cons:__
*  You have to take care of everything:
   *  SLA
   *  Security
   *  Availability
   *  Backup

## Azure SQL
* Managed SQL server on Azure
* Great compatibility with on-prem SQL server
* Flexible pricing model
* Azure SQL Flavor

## Azure SQL Flavor
### Azure SQL Database
* Managed SQL Server on Azure
* Single Database on a single server
* Automatic backups, updates, scaling
* Good compatibility with on-prem SQL server
__Security:__
* IP firewall rules
* Service Endpoints
* SQL and Azure AD Authentication
* Secure communication (TLS)
* Data encrypted by default (TDE: Transparent Data Encryption)
__Backup:__
* Full: Every week
* Differential: Every 12-24 hrs
* Transaction Log: Every 5-10 mins
__Retention Period:__
* Regular: 7-35 days
* Long term: up to 10 yrs
__Availability:__
* Backup stored in a geo redundant storage
* Active geo replication
* SLA: 99.9% - 99.995% depends on tier and redundancy
__Compute Tiers:__

| Provisioned | Serverless |
|------|-------|
| Pay for allocated resources regardless of actual use | Pay for actual use -vCore + RAM/s |
| Can be reserved | Automatically paused when inactive (pay just for storage) |
| | Slight delay when warming up |
| | Cannot be reserved

### Elastic Pool
* Based on Azure SQL
* Allow storing multiple databases on single server
* Cost effective
* Good for mutual DB, which are not requested on the same time (show consumption pic below):
<br/>![spike-diagramm](images/spike-diagramm.png)

### Managed Instance
* Closer to the On-Prem SQL Server
* Can be deployed t VNet
* Business close to on-prem use
* Main difference with other flavor:
  * No active geo replication
  * SLA: 99.99%
  * Support built-in function
  * Run CLR code
  * No auto scaling and tuning
  * No availability zone
  * No serverless tier
  * No Hyperscale

## Princing

## Which Azure SQL to chose
* Migration -> Managed Instance
* Multiple mostly low-utilization DBs -> Elastic Pool
* All other cases -> Azure SQL
