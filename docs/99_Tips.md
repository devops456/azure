# Tips

## Activate  a resource
If you don't have access resource go to :
1. __Subscription__ in the search bar
2. __Settings/Resource providers__ in the left menu
3. Search for the resource you need. In my case Microsoft.AAD
4. Register
5. Check using: `az provider show --namespace Microsoft.AAD -o table`
<br/>You should the following output:
```bash
> $ az provider show --namespace Microsoft.AAD -o table
Namespace      RegistrationPolicy    RegistrationState
-------------  --------------------  -------------------
Microsoft.AAD  RegistrationRequired  Registered
```

## Disk
if your VM needs more disks, in addition to the default one provided by Azure, then go to the Disks page for that, and add whatever disk you need. Don't forget that disks have costs, and check it in the calculator beforehand

## VM back up
Want to backup your VM so that it can be restored in a case of failure ? Check the Backup page, where you can define the frequency  of the backup and retention period

## DNS
You can define DNS name for the VM, so that it will be accessible not just using its IP. This can be done by clicking the DNS Name: Configure link in the Overview page

## Show sum up chart by Resource Group
Go to Resource group and Resource Visualizer: <br/>
![rg-visualizer](./images/rg-visualizer.png)

## Application Gateway affinity
Affinity is set within HTTP settings of an App Gateway it means that a user will use the same VM all along is session. That can be a problem for a load balancer and should be avoid as much as possible.
<br/>It also highlight that our Application is stateful, which is better to avoid.