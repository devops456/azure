# Networking

## Why ?
__Security__

Example of threats:
* Brut force attacks on 3389(RDP)
* No line of defense in front of the VM web server

## 4 networking-related cloud services
* [Vnets](05.1_vnets.md)
* [SubNets](05.2_subnets.md)
* [Load Balancer](05.3_loadbalancer.md)
* [Application gateway](05.4_Application_gateway.md)

## Secure VM access

### JIT Access
Just In Time access:
* Opens the port access on demand, and automatically closes it
* Can be configured from the VM's page in portal
* Requires Security Center License Upgrade

### VPN
Secure tunnel to the VPN
* Can be configured 
* Requires VPN software and license (not part of Azure)

### Jump Box
Place another VM in the VM:
* Allow access only to this VM
* When need to access a VM, reach it through the Jumb Box (port dedicated)
* Cost: The additional VM

### Bastion
A web-based connection to the VM:
* No open port is required
* Simple and secure
* Cost: ~ 140$ / month

## Service Endpoint and Private Link

Issue: Resources that are only reached by cloud resources. Example:
* Backend <-> Database

Might pose a security risk solved by Security Endpoint or Private link:
* Creates a route from th e VNet to the managed service

### Service Endpoint

* The traffic never leaves Azure Backbone 
  * Although the resource still has a __public IP__
* Free
* Enable Service Endpoint on the Subnet

_N.B: Connect to the service using **Public IP** through Azure backbone_

### Private Link
* Extends the managed service to the VNet
* The traffic never leaves the VNet
* Access from the internet can be blocked
* Can be used from on-prem networks
* Not Free
* How it's done
  * Configure the resource to connect to the VNet
  * Configure DNS

_N.B: Connection to the service using **Private IP**_

### Benchmark

|                      |    Service Endpoint    |      Private link      |
|----------------------|------------------------|------------------------|
|          Age         |         Legacy         |         Newer          |
|        Security      | Connects via Public IP | Connect via private IP |
|       Simplicity     |       Very simple      |       More complex     |
|        Price         |        Free            |       Not Free         |
|  Supported services  |     Limited list       | Large list (growing)   |
| On-prem connectivity |      Quite complex     |       Supported        |


## App Service Access Restriction
* Similar to NSG but for App Service
* Restricts traffic to the App Service
* By default all inbound traffic is allowed (in revelant port)
* Using access restrictions inbound traffic is restricted to the allowed IPs, VNet, Service Tag
* Main use cases:
  * Backnd App Service that should be accessed from frontend AppService, VM only
  * App Service that sits behind Application Gateway/Load Balancer and shouldn't be accessible directly
  * Less common App Service to a specific client (stagging)

## App Service Environment: ASE
* Special type of app service deployed directly to a dedicated VNet
* VNet can be configured like any other VNet - Subnet, NSGs ...
* Created on dedicated hardware
* Quite expensive
* Major Use case
  * Elevated security - complete isolation
  * Very high scale requirement


## Typical Secure Network Design
Hub and Spoke Desing

![secure-net-desing](images/secure-net-design.png)