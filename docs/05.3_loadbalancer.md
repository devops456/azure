# Load Balancer
* Azure service that distributes load and checks health of VMs
* When a VM is not healthy, no traffic is directed to it
* Can work with VMs or Scale Set
* Can be private or public
* Operates at layer 4 of OSI model
* Distribution is set considering 5 parameters:
  * Source IP
  * Source Port
  * Destination IP
  * Destination Port
  * Protocol type

## 2 types of load balancer
|        Basic         |        Standard        |
|----------------------|------------------------|
|    No redundancy     |     Redundancy         |
|    Open by default   |   Secure by default    |
|  Up to 300 instances |  Up to 1000 instances  |
|         No SLA       |      99.99%            |
|       Free           |        Not Free        |

## 4 Main configurations:
* Frontend IP Configuration: Public IP exposed by the load balancer
* Backend pools: Connected VMs
* Health probes: Check VMs' health
* Load balancing rules: A rule connecting Frontend with Backend pool

## Healths probes
* Check the health of the VM
* A non-healthy VM will be marked as down and will not be routed to
* Run in intervals (usually a few seconds)
* Can run on TCP, HTTP, HTTPS (Standard only)
* Runon th VM's host
* No network traffic outside the host
* Originate from the same IP
* Allowed y default in NSG

## Use Case
* For internal resource
* Do not use for external resources
  * Especially on Web Apps, Web API...
  * Can't handle HTTP
  * Doesn't route based on path
  * No protection
* For this we have the Application gateway
