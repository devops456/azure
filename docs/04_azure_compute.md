# Azure Compute

4 types of Compute services:
* [Virtual Machine](04.1_Virtual_Machines.md)
* [App Service](04.2_App_services.md)
* [AKS](04.3_aks.md)
* [Azure Function](04.4_azure_functions.md)

## [Sum up section](04.5_sumup.md)

## Other compute option
* Logic App: Workflow interact with azure services
* ACI - Azure Container Instance (docker containers)
* App Services Container - Deploy docker to App Service