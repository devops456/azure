# AZURE

## [Introduction to the cloud](docs/01_introduction_to_the_cloud.md)

## [Introduction to AZURE](docs/02_introduction_to_azure.md)

## [Introducing our app](docs/03_introducing_our_app.md)

## [Azure Compute](docs/04_azure_compute.md)

## [Networking](docs/05_networking.md)

## [Data](docs/06_data.md)

## [Azure Active Directory](docs/07_azure_active_directory.md)

